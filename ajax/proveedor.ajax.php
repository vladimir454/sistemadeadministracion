
<?php

require_once "../controladores/proveedor.controlador.php";
require_once "../modelos/proveedores.modelo.php";

class AjaxProveedor 
{
    //editar cliente

    public $idProveedor;

    public function ajaxEditarProveedor()
    {
        $item = "id";
        $valor = $this->idProveedor;

        $respuesta = ControladorProveedores::ctrMostrarProveedores($item, $valor);
        
        echo json_encode($respuesta);
    }
}
/*=============================================
EDITAR CLIENTE
=============================================*/	

if(isset($_POST["idProveedor"])){

	$cliente = new AjaxProveedor();
	$cliente -> idProveedor = $_POST["idProveedor"];
	$cliente -> ajaxEditarProveedor();
    
}
