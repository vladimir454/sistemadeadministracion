<?php

class ControladorProveedores{
    /*=============================================
	CREAR PROVEEDOR
    =============================================*/
    
    static public function ctrCrearProveedor()
    {   
        
        if (isset($_POST["nuevoProveedor"])) {
            
            if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoProveedor"]) &&
                preg_match('/^[0-9]+$/', $_POST["nuevoDocumento"]) &&
                preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["nuevoEmail"]) && 
                preg_match('/^[()\-0-9 ]+$/', $_POST["nuevoTelefono"]) && 
                preg_match('/^[#\.\-a-zA-Z0-9 ]+$/', $_POST["nuevaDireccion"])){
            
            $tabla = "proveedores";

            $datos = array("nombre"=>$_POST["nuevoProveedor"],
                            "documento"=>$_POST["nuevoDocumento"],
                            "email"=>$_POST["nuevoEmail"],
                            "telefono"=>$_POST["nuevoTelefono"],
                            "direccion"=>$_POST["nuevaDireccion"],
                            "fecha_nacimiento"=>$_POST["nuevaFechaNacimiento"]);
            
            $respuesta = ModeloProveedor::mdlIngresarProveedor($tabla, $datos);
            
            if($respuesta == "ok"){

                echo'<script>

                swal({
                      type: "success",
                      title: "El proveedor se agrego",
                      showConfirmButton: true,
                      confirmButtonText: "Cerrar"
                      }).then(function(result){
                                if (result.value) {

                                window.location = "proveedores";

                                }
                            })

                </script>';
                

            }

        }else{

            echo'<script>

                swal({
                      type: "error",
                      title: "¡El proveedor no puede ir vacío o llevar caracteres especiales!",
                      showConfirmButton: true,
                      confirmButtonText: "Cerrar"
                      }).then(function(result){
                        if (result.value) {

                        window.location = "proveedores";

                        }
                    })

              </script>';
              


            }

        }

    }

    /*=============================================
	MOSTRAR PROVEEDORES
	=============================================*/

    static public function ctrMostrarProveedores($item, $valor)
    {
        $tabla = "proveedores";
        $resp = ModeloProveedor::mdlMostrarProveedores($tabla, $item, $valor);
        
        return $resp;

    }
    
    static public function ctrEditar()
    {
       
        
        if(isset($_POST["editarproveedor"])){
            
			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarproveedor"]) &&
			   preg_match('/^[0-9]+$/', $_POST["editarDocumento"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["editarEmail"]) && 
			   preg_match('/^[()\-0-9 ]+$/', $_POST["editarTelefono"]) && 
			   preg_match('/^[#\.\-a-zA-Z0-9 ]+$/', $_POST["editarDireccion"])){
               
			   	$tabla = "proveedores";
                
			   	$datos = array("id"=>$_POST["idProveedor"],
			   				   "nombre"=>$_POST["editarproveedor"],
					           "documento"=>$_POST["editarDocumento"],
					           "email"=>$_POST["editarEmail"],
					           "telefono"=>$_POST["editarTelefono"],
					           "direccion"=>$_POST["editarDireccion"],
					           "fecha_nacimiento"=>$_POST["editarFechaNacimiento"]);
                                           
        $resp = ModeloProveedor::mdlEditarProveedor($tabla, $datos);
     
        if($resp == "ok"){
            
            echo'<script>

            swal({
                  type: "success",
                  title: "El proveedor ha sido cambiado correctamente",
                  showConfirmButton: true,
                  confirmButtonText: "Cerrar"
                  }).then(function(result){
                            if (result.value) {

                            window.location = "proveedores";

                            }
                        })

            </script>';

        }

        }else{

            echo'<script>

                swal({
                    type: "error",
                    title: "¡El proveedor no puede ir vacío o llevar caracteres especiales!",
                    showConfirmButton: true,
                    confirmButtonText: "Cerrar"
                    }).then(function(result){
                        if (result.value) {

                        window.location = "proveedores";

                        }
                    })

            </script>';

            }
        }
    }
    static public function ctrEliminar()
    {
        if (isset($_GET['idProveedor'])) {
            # code...
            $tabla = "proveedores";
            $datos = $_GET["idProveedor"];
           
            $respuesta = ModeloProveedor::mdlEliminarProveedor($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "El proveedor ha sido borrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result){
								if (result.value) {

								window.location = "proveedores";

								}
							})

				</script>';
            }

        }
    }
}

?>